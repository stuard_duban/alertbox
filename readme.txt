=== Matomo Analytics - Ethical Stats. Powerful Insights. ===
Contributors: stuard
Donate link: 
Tags: alertbox
Requires at least: 4.8
Tested up to: 5.3
Stable tag: 1.0.2
Requires PHP: 5.3
License: GPLv3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html