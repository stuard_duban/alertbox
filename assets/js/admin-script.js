jQuery(document).ready(function() {
(function($) {
    $('.copyShortcodeButton').click(function() {
        $input = $(this).parent().find('input');
        copyShortcode($input.attr('id'));
    });

    $('.shortcode-examples input').click(function() {
        copyShortcode($(this).attr('id'));
    });
    
    
    function copyShortcode(input) {
        var copyText = document.getElementById(input);
        copyText.select();
        copyText.setSelectionRange(0, 99999)
        document.execCommand("copy");
        alert("Copied the text: " + copyText.value);
      }
	
})( jQuery );
})

