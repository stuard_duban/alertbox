<div class="bootstrap-alert-wrap">
    <div class="container shortcode-metabox">
        <div class="row">
            <div class="col-12">
                <h2>
                    <?php 
                        _e( 'How to use it?', 'alertbox' );
                    ?>
                </h2>
                <p>
                    <?php 
                        _e( 'Copy the alert box shortcode by clicking on the button and paste it where you need.', 'alertbox' );
                    ?>
                </p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 col-sm-4">
                <div class="col-12">
                    <div class="alert alert-primary alert-block">
                        <?php _e( 
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam malesuada aliquet. Fusce convallis leo at malesuada finibus. Curabitur ullamcorper nisl in ipsum maximus, id faucibus lorem tempor.', 
                            'alertbox' 
                            ); 
                        ?>
                    </div>
                </div>
                <div class="col-12">
                    <input type="text" class="form-control" value="[alert type='primary']<?php _e('Put your text here', 'alertbox')?>[/alert]" id="inputAlertPrimary" readonly>
                    <button class="btn btn-info mt-3 copyShortcodeButton" id="buttonAlertPrimary" ><?php _e( 'Copy Shortcode', 'alertbox' ); ?></button>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="col-12">
                    <div class="alert alert-warning alert-block">
                    <?php _e( 
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam malesuada aliquet. Fusce convallis leo at malesuada finibus. Curabitur ullamcorper nisl in ipsum maximus, id faucibus lorem tempor.', 
                            'alertbox' 
                            ); 
                        ?>
                    </div>
                </div>
                <div class="col-12">
                    <input type="text" class="form-control" value="[alert type='warning']<?php _e('Put your text here', 'alertbox')?>[/alert]" id="inputAlertWarning" readonly>
                    <button class="btn btn-info mt-3 copyShortcodeButton" id="buttonAlertWarning" ><?php _e( 'Copy Shortcode', 'alertbox' ); ?></button>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="col-12">
                    <div class="alert alert-danger alert-block">
                    <?php _e( 
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam malesuada aliquet. Fusce convallis leo at malesuada finibus. Curabitur ullamcorper nisl in ipsum maximus, id faucibus lorem tempor.', 
                            'alertbox' 
                            ); 
                        ?>
                    </div>
                </div>
                <div class="col-12">
                    <input type="text" class="form-control" value="[alert type='danger']<?php _e('Put your text here', 'alertbox')?>[/alert]" id="inputAlertDanger" readonly>
                    <button class="btn btn-info mt-3 copyShortcodeButton" id="buttonAlertDanger" ><?php _e( 'Copy Shortcode', 'alertbox' ); ?></button>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 col-sm-4">
                <div class="col-12">
                    <div class="alert alert-success alert-block">
                    <?php 
                        _e( 
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam malesuada aliquet. Fusce convallis leo at malesuada finibus. Curabitur ullamcorper nisl in ipsum maximus, id faucibus lorem tempor.', 
                            'alertbox' 
                        ); 
                    ?>
                    </div>
                </div>
                <div class="col-12">
                    <input type="text" class="form-control" value="[alert type='success']<?php _e('Put your text here', 'alertbox')?>[/alert]" id="inputAlertSucccess" readonly>
                    <button class="btn btn-info mt-3 copyShortcodeButton" id="buttonAlertSuccess" ><?php _e( 'Copy Shortcode', 'alertbox' ); ?></button>
                </div>
            </div>
        
            <div class="col-12 col-sm-4">
                <div class="col-12">
                    <div class="alert alert-muted alert-block">
                    <?php 
                        _e( 
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam malesuada aliquet. Fusce convallis leo at malesuada finibus. Curabitur ullamcorper nisl in ipsum maximus, id faucibus lorem tempor.', 
                            'alertbox' 
                        ); 
                    ?>
                    </div>
                </div>
                <div class="col-12">
                    <input type="text" class="form-control" value="[alert type='muted']<?php _e('Put your text here', 'alertbox')?>[/alert]" id="inputAlertMuted" readonly>
                    <button class="btn btn-info mt-3 copyShortcodeButton" id="buttonAlertMuted" ><?php _e( 'Copy Shortcode', 'alertbox' ); ?></button>
                </div>
            </div>

            <div class="col-12 col-sm-4">
                <div class="col-12">
                    <div class="alert alert-info alert-block">
                    <?php 
                        _e( 
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In aliquam malesuada aliquet. Fusce convallis leo at malesuada finibus. Curabitur ullamcorper nisl in ipsum maximus, id faucibus lorem tempor.', 
                            'alertbox' 
                        ); 
                    ?>
                    </div>
                </div>
                <div class="col-12">
                    <input type="text" class="form-control" value="[alert type='info']<?php _e('Put your text here', 'alertbox')?>[/alert]" id="inputAlertInfo" readonly>
                    <button class="btn btn-info mt-3 copyShortcodeButton" id="buttonAlertInfo" ><?php _e( 'Copy Shortcode', 'alertbox' ); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>