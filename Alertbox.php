<?php
/**
* @package NASA_astronautas
*/
/**
 * Plugin Name: AlertBox
 * Plugin URI:        
 * Description:       AlertBox shortcodes based on Bootstrap 4
 * Version:           1.0.0
 * Requires PHP:      7.3.4
 * Author:            Stuard Romero
 * License:			  GPLv2 or later
 * Text Domain:       alertbox
 */

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2005-2015 Automattic, Inc.
*/

if ( !defined( 'ABSPATH' ) ) {
	die;
}

class Alertbox
{
	public function __construct()
	{
		add_action('init', array($this, 'loadTranslation'));

		add_action('admin_menu', array($this, 'adminMenuSection'));

		add_shortcode( 'alert', array($this, 'showShortcode') );

		$this->createMetaboxes();
	}

	function loadTranslation() {
		load_plugin_textdomain( 'alertbox', false, 'alertbox/languages' );
	}
	  
	
	public function activate()
	{
		flush_rewrite_rules();
	}

	public function deactivate() 
	{
		flush_rewrite_rules();
	}
	
	public function adminMenuSection() 
	{ 
		add_menu_page( 
			'AlertBoxes Shortcodes', 
			'Alertboxes', 
			'edit_posts', 
			'alertbox', 
			array($this, 'alertBoxShortcodes'), 
			'dashicons-media-spreadsheet' 
		);
	}

	public function alertBoxShortcodes()
	{
		include plugin_dir_path( __FILE__ ) . "/templates/shortcode_list.php";
	}

	public function showShortcode($atts = [], $content = null)
	{
		if (!is_null($content) && isset($atts['type'])) {
			$type = $atts['type'];
			ob_start();
			include plugin_dir_path( __FILE__ ) . "/templates/alert.php";
			return ob_get_clean();
		}
	}

	public function createMetaboxes()
	{
		add_filter( 'add_meta_boxes', array( $this, 'registrarMetabox') );
	}

	public function registrarMetabox() 
	{
		$shortcodeList = array($this, 'shortcodeList');
		$screen = array('page', 'post');
		add_meta_box('shortcodes', "Alert box Shortcodes", $shortcodeList, $screen);

	}

	public function shortcodeList() {
		include plugin_dir_path( __FILE__ ) . "/templates/alert_metabox.php";
	}

	public function enqueueFront() {
		//Encolar estilos para el admin
		wp_enqueue_style('style', plugins_url('/assets/css/style.css', __FILE__ ));

		//ecolar estilos
		wp_enqueue_style('bootstrap-min', plugins_url('/assets/css/bootstrap.min.css', __FILE__ ));

		wp_enqueue_style('font-awesome', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css");
	}

	/*
	* Enquee js y css
	*/
	public function registerAssets()
	{
		add_action('admin_enqueue_scripts', array($this, 'enqueueAdmin'));
		add_action('wp_enqueue_scripts', array($this, 'enqueueFront'), 50);
	}

	/*
	* enqueueAdmin
	* Funcion para encolar assets en el admin
	*/
	public function enqueueAdmin() 
	{
		//Encolar estilos para el admin
		wp_enqueue_style('admin_styles', plugins_url('/assets/css/admin-styles.css', __FILE__ ));

		wp_enqueue_style('bootstrap-min', plugins_url('/assets/css/bootstrap.min.css', __FILE__ ));

		wp_enqueue_script( 'admin_script', plugins_url('/assets/js/admin-script.js', __FILE__ ), array( 'jquery' ), false, false );
	}

}

if( class_exists('Alertbox') ) {
	$alertBox = new Alertbox();

	$alertBox->registerAssets();
}

//Activación del plugin
register_activation_hook( __FILE__, array($alertBox, 'activate'));

//Desactivación del plugin
register_deactivation_hook( __FILE__, array($alertBox, 'deactivate'));

//Desinstalación del plugin
register_uninstall_hook( __FILE__, 'uninstall' );

//register_uninstall_hook($this->Path_Get('plugin_file_index'), 'Uninstall');

